/**
 * Created with JetBrains RubyMine.
 * User: wuhuizuo
 * Date: 14-1-3
 * Time: 下午3:07
 * To change this template use File | Settings | File Templates.
 */

function set_suite_dialog(title, data, opr) {
    var dialog_str = 'div#suite_dialog';
    $(dialog_str.concat(' #suite_label')).html(title);
    for (var key in data) {
        $(dialog_str.concat(' [name=', key, ']')).val(data[key]);
    }
    $('div#suite_dialog [name]').prop('readonly', opr == 'del');
    $('div#suite_dialog [name=opr]').val(opr);
}

function parse_suite_data(btn) {
    var cells = $('td', $(btn).closest('tr'));
    return {
        name: $(cells[1]).text(),
        old_name: $(cells[1]).text(),
        desc: $(cells[2]).text(),
        old_desc: $(cells[2]).text()
    };
}

$(document).ready(function () {
    $('.btn.btn-primary.submit-trigger').click(function () {
        $(this).parent().prev('.modal-body').find('form').submit();
    });
    $('.add_suite').on('click', function (event) {
        event.preventDefault();
        set_suite_dialog('添加 测试套件', parse_suite_data(this), 'add');
    });
    $('.edit_suite').on('click', function (event) {
        event.preventDefault();
        set_suite_dialog('编辑 测试套件', parse_suite_data(this), 'edit');
    });
    $('.del_suite').on('click', function (event) {
        event.preventDefault();
        set_suite_dialog('删除 测试套件', parse_suite_data(this), 'del');
    });
});
