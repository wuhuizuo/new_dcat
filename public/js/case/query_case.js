/**
 * Created with JetBrains RubyMine.
 * User: wuhuizuo
 * Date: 14-1-4
 * Time: 下午5:25
 * To change this template use File | Settings | File Templates.
 */

function post_case_data(is_need_reload) {
    var caseDataNameIds = new Array();
    var caseDataDateIds = new Array();
    var caseDataDataIds = new Array();
    var nameIdPat = new RegExp("^case_data_[0-9]+_name$");
    var dateIdPat = new RegExp("^case_data_[0-9]+_date$");
    var dataIdPat = new RegExp("^case_data_[0-9]+_data$");
    var inputs = document.getElementsByTagName("input");
    var tables = document.getElementsByTagName("table");
    for (var i = 0; i < inputs.length; i++) {
        var id = inputs[i].id;
        if (nameIdPat.test(id))
            caseDataNameIds.push(id);
        if (dateIdPat.test(id))
            caseDataDateIds.push(id);
    }
    for (var i = 0; i < tables.length; i++) {
        var id = tables[i].id;
        if (dataIdPat.test(id))
            caseDataDataIds.push(id);
    }
    caseDataNameIds.sort;
    caseDataDateIds.sort;
    caseDataDataIds.sort;
    var datas = new Array()
    for (var i = 0; i < caseDataNameIds.length; i++) {
        var data = new Object();
        data.table_name = document.getElementById(caseDataNameIds[i]).value;
        data.table_date = document.getElementById(caseDataDateIds[i]).value;
        data.table_datas = getTableArray(caseDataDataIds[i]);
        datas.push(data);
    }
    save_query_data({edit_part: 'case_data', record_datas: JSON.stringify(datas)}, is_need_reload, 'div_data');
}

function post_case_config(config_table_id, is_need_reload) {
    save_query_data({
        edit_part: 'case_config',
        case_configs: JSON.stringify(getTableArray(config_table_id))
    }, is_need_reload, 'div_cfg');
}

function post_case_check(check_table_id, is_need_reload) {
    save_query_data({
        edit_part: 'case_check',
        case_checks: JSON.stringify(getTableArray(check_table_id))
    }, is_need_reload, 'div_check');
}
function post_case_summary(is_need_reload) {
    save_query_data({
        edit_part: 'summary',
        case_name: $('input[name=case_name]').val(),
        case_webname: $('input[name=case_webname]').val()
    }, is_need_reload, 'div_summary');
}

function init_check() {
    save_query_data({edit_part: 'init_check'}, true, '');
}

function init_config() {
    save_query_data({edit_part: 'init_config'}, true, '');
}

function add_case_data(table_name) {
    save_query_data({edit_part: 'add_case_data', table_name: table_name}, true, '');
}

function save_query_data(addtion_data, is_need_reload, div_id) {
    var url = purl();
    var init_data = {
        case_id: url.param('id'),
        tc_type: url.param('type'),
        suite_name: url.param('suite')
    };
    $.ajax({
        type: "post",
        data: $.extend({}, init_data, addtion_data),
        async: false,
        success: function () {
            if (div_id.length > 0) $('div#' + div_id).removeClass('panel-danger').addClass('panel-info');
            if (is_need_reload) document.location.reload(true);
        }
    });
}

function post_query_all() {
    post_case_summary(false);
    post_case_data(false);
    post_case_config('case_config', false);
    post_case_check('case_check', false);
    document.location.reload(true);
}

function monitor_query_tc_part(div_id) {
    var tc_part = $('div#' + div_id);
    var tc_config_text = tc_part.data('text');
    var tc_config_text_real = tc_part.text();
    if (tc_config_text && tc_config_text != tc_config_text_real) {
        tc_part.trigger('query_tc_change');
    }
    else if (!tc_config_text) {
        tc_part.data('text', tc_part.text());
    }
}

function dange_when_div_changed(div_id) {
    $('div#' + div_id).on('query_tc_change', function () {
        $(this).removeClass('panel-info').addClass('panel-danger');
    });
}

function alert_when_living_and_not_save() {
    $(window).bind('beforeunload', function () {
        if ($('div.panel-danger').length > 0) {
            return '你还有未保存的用例修改,留在此页可保存其它部分.';
        }
    });
}

function clone_data_table(clone_btn) {
    var table_view = $(clone_btn).closest('div[id^=div_data_table]');
    var tc_data_view = $(table_view).closest('div#div_data .panel-body');
    var table_count = $('div[id^=div_data_table]').length;
    var new_table_view_inner = table_view.html().replace(/_table\d+\b/g, '_table' + table_count);
    new_table_view_inner = new_table_view_inner.replace(/\bcase_data_\d+_/g, 'case_data_' + table_count + '_');
    var new_table_view = '<div class="panel panel-info" id="div_data_table' +
        table_count + '">' + new_table_view_inner + '</div>';
    tc_data_view.append(new_table_view);
}

function del_data_table(del_btn) {
    $(del_btn).closest('div[id^=div_data_table]').remove();
}
$(document).ready(function () {
    setInterval("monitor_query_tc_part('div_summary')", 1000);
    dange_when_div_changed('div_summary');
    setInterval("monitor_query_tc_part('div_data')", 1000);
    dange_when_div_changed('div_data');
    setInterval("monitor_query_tc_part('div_cfg')", 1000);
    dange_when_div_changed('div_cfg');
    setInterval("monitor_query_tc_part('div_check')", 1000);
    dange_when_div_changed('div_check');
    alert_when_living_and_not_save();
});