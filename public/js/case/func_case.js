/**
 * Created with JetBrains RubyMine.
 * User: wuhuizuo
 * Date: 14-1-4
 * Time: 下午5:25
 * To change this template use File | Settings | File Templates.
 */

//调试运行用例
function init_debug_func_tc_view(debug_code) {
    $('#debug_log_dialog').modal('show');
    var code_editor = ace.edit('debug_code');
    code_editor.setValue(debug_code);
    code_editor.getSession().setMode("ace/mode/ruby");
    ace.edit('debug_log').setValue('');
    var debug_button = $('div#debug_log_dialog button.btn-primary');
    debug_button.prop('disabled', false)
    //获取调试执行机        
    $.ajax({
        url: '/cfgs/json_list_drblist',
        type: 'get',
        dataType: 'json',
        success: function (json) {
            var drb_select =  $('div#debug_log_dialog select#debug_drb');
            drb_select.html('');
            $.each(json, function (i, uri) {
                $.ajax({
                    url: '/tools/test_dr',
                    type: 'get',
                    async: false,
                    data: {uri: uri},
                    success: function (data) {
                        if (data == 'online') drb_select.append('<option>' + uri + '</option>');                        
                    }
                });
            });            
            if (drb_select.val() == null)  debug_button.prop('disabled', true);
        }
    });
    //获取测试设备    
    $.ajax({
        url: '/cfgs/device_cfg_names',
        type: 'get',
        dataType: 'json',
        success: function (json) {
            var cfg_select = $('div#debug_log_dialog select#debug_cfgname');
            cfg_select.html('');
            $.each(json, function (i, cfg_name) {
                cfg_select.append('<option>' + cfg_name + '</option>');
            });            
            if (cfg_select.val() == null)  debug_button.prop('disabled', true);
        }
    });        
}

function debug_run() {
    //下发调试(异步)
    var code = ace.edit('debug_code').getValue();
    var drb = $('div#debug_log_dialog select#debug_drb').val();
    var cfg_name = $('div#debug_log_dialog select#debug_cfgname').val();
    var update_log = function() {
        $.ajax({
            type: 'get',
            url: '/monitors/running_log',
            data: { uri: drb},
            success: function (data) {
                var log_view = ace.edit('debug_log');
                log_view.insert(data);               
            }
        });
    }  

    $.ajax({
        type: 'get',
        url: '/cfgs/device_json',
        dataType: 'json',
        data: { cfg_name: cfg_name },
        success: function (cfg_json) {
           var updater = window.setInterval("update_log()", 1000); 
           $.ajax({
                type: 'post',
                url: '/tests/case_debug',
                data: {
                    code: code,
                    drb: drb,
                    cfg: cfg_json 
                },
                success: function (data) {
                    window.clearInterval(updater);
                    update_log();
                    //显示执行结果
                }
            });
        }
    });
}


function steps2code(steps) {
    var code;
    $.ajax({
        type: 'post',
        async: false,
        url: '/tc_snipper/steps_to_code',
        data: {steps: steps},
        success: function (data) {
            code = data;
        }
    });
    return code;
}

function save_func_tc() {
    var url = purl();
    var suite = url.param('suite');
    var tc_type = url.param('type');
    var case_id = url.param('id');
    var tc_name = $("input#case_name").val();
    var tc_steps = ace.edit("steps").getValue();
    var tc_code = steps2code(tc_steps);
    $.ajax({
        type: "post",
        data: {
            case_name: tc_name,
            steps: tc_steps,
            code: tc_code,
            case_id: case_id,
            tc_type: tc_type,
            suite_name: suite
        },
        success: function (data) {
            document.location.reload(true);
        }
    });
}

function display_snipper(editor, snipper) {
    var lines = editor.getValue().split("\n");
    var cur_l = editor.getCursorPosition()["row"]
    var context = lines.slice(0, cur_l + 1).join("\n")
    $.ajax({
        url: "/tc_snipper",
        type: "post",
        data: {context: context},
        success: function (data) {
            snipper.setValue(data);
        }
    });
}

function toggle_list_and_detail() {
    $('#tc_att_list').slideToggle();
    $('#tc_att_detail').slideToggle();
}

function reload_att_list(need_toggle) {
    var url = purl();
    $('div#tc_att_list').html('<span class="glyphicon glyphicon-time">loading...</span>');
    if (need_toggle) toggle_list_and_detail();
    $.ajax({
        type: 'get',
        url: 'case_att_list',
        data: {
            id: url.param('id'),
            type: url.param('type'),
            suite: url.param('suite')
        },
        success: function (data) {
            $('#tc_att_list').html(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#tc_att_list').html("Status: " + textStatus + '<br><hr>' + "Error: " + errorThrown);
        }
    });
}

function load_att(att_name, no_fresh) {
    var url = purl();
    $('#tc_att_detail').html('<span class="glyphicon glyphicon-time">loading...</span>');
    if (!no_fresh) toggle_list_and_detail();

    $.ajax({
        type: 'get',
        url: 'case_att',
        data: {
            id: url.param('id'),
            type: url.param('type'),
            suite: url.param('suite'),
            att_name: att_name
        },
        success: function (data) {
            $('#tc_att_detail').html(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#tc_att_detail').html("Status: " + textStatus + '<br><hr>' + "Error: " + errorThrown);
        }
    });
}

function init_editor(readonly) {
    var snipper = ace.edit("snipper");
    snipper.setValue("用例写作辅助提示在这里^_^\n使用'Alt' + '/'可以输出当前位置的提示。");
    snipper.setReadOnly(true);
    var editor = ace.edit("steps");
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode("ace/mode/zwh");
    editor.setReadOnly(readonly);
    editor.commands.addCommand({
        name: 'get_snipper',
        bindKey: {win: 'Alt-/', linux: 'Alt-/', mac: 'Command-M'},
        exec: function (editor) {
            display_snipper(editor, snipper);
        },
        readonly: false
    });
    editor.getSession().selection.on('changeCursor', function (e) {
        display_snipper(editor, snipper);
    })
    var coder = ace.edit("code");
    coder.setTheme("ace/theme/monokai");
    coder.getSession().setMode("ace/mode/ruby");
    coder.setReadOnly(true);
}

function uploadAttFile() {
    var file = document.getElementById('att_file_name').files[0]; //Files[0] = 1st file
    var reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = function (event) {
        var result = event.target.result;
        var att_name = $('#att_name').val();
        var fileName = document.getElementById('att_file_name').files[0].name; //Should be 'picture.jpg'
        var url = purl();
        var post_params = {
            type: url.param('type'),
            suite: url.param('suite'),
            id: url.param('id'),
            opr: 'update_file',
            att_file_data: result,
            att_name: att_name,
            att_file_name: fileName
        };
        $.post('case_att', post_params, function () {
            load_att(att_name, true);
        });
    };
    reader.onloadstart = function () {
        var init_progress_bar = '<div id="att_file_upload_progress" class="progress-bar progress-bar-success" role="progressbar" style="width: \"0%\"">0%</div>';
        $('#att_file_upload_status').html(init_progress_bar);
    };
    reader.onprogress = function (event) {
        var percent = Math.round((event.loaded / event.total) * 100) + '%';
        $('#att_file_upload_progress').html(percent);
        $('#att_file_upload_progress').css('width', percent);
    };
    reader.onloadend = function () {
        $('#att_file_upload_status').html('上传完毕.');
    };
}

/*
 * 保存csv附件或者csv数据类型的表格编辑结果
 * 如果是csv_string数据类型,则直接更新其value,
 * 如果是csv_file数据类型,则更新其对应value路径文件中的内容
 */
function save_csv_att() {
    var att_name = $('#att_name').val();
    var url = purl();
    var post_params = {
        type: url.param('type'),
        suite: url.param('suite'),
        id: url.param('id'),
        opr: 'update_csv_data',
        att_name: att_name,
        att_value: JSON.stringify(getTableArray('csv_table'))
    };
    $.post('case_att', post_params, function () {
        load_att(att_name, true);
    });
}

/*
 * 保存字符串数据类型的编辑结果
 */
function save_string_att() {
    var att_name = $('#att_name').val();
    var url = purl();
    var post_params = {
        type: url.param('type'),
        suite: url.param('suite'),
        id: url.param('id'),
        opr: 'update_string',
        att_name: att_name,
        att_value: $('textarea#att_value').val()
    };
    $.post('case_att', post_params, function () {
        load_att(att_name, true);
    });
}


/*
 * 删除指定数据驱动项目
 */
function del_att() {
    var att_name = $('#att_name').val();
    var url = purl();
    var post_params = {
        type: url.param('type'),
        suite: url.param('suite'),
        id: url.param('id'),
        opr: 'del',
        att_name: att_name
    };
    $.post('case_att', post_params, function () {
        reload_att_list(true);
    });
}

function add_att() {
    var att_name = $('#add_att_name').val();
    var url = purl();
    var post_params = {
        type: url.param('type'),
        suite: url.param('suite'),
        id: url.param('id'),
        opr: 'add',
        att_name: att_name,
        att_type: $('#add_att_type').val()
    };
    $.post('case_att', post_params, function () {
        reload_att_list(false);
    });
}