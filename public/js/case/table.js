/**
 * Created by wuhuizuo on 14-2-26.
 */

//获取表格内容成二维数组
function getTableArray(table_id) {
    var table = document.getElementById(table_id)
    if (table == null) {
        return new Array(0);
    }
    else {
        var rows = table.rows;
        var row_count = rows.length;
        var table_array = new Array(row_count);
        var col_names = rows[0].cells;
        for (var i = 0; i < row_count; i++) {
            var row = rows[i].cells;
            table_array[i] = new Object();
            var k = 0
            for (var j = 0; j < row.length; j++) {
                if ($(col_names[j]).text() != '#') {
                    table_array[i][k] = $(row[j]).text();
                    k++;
                }
            }
        }
        return table_array;
    }
}

//表格增加列
function addColumn(tblId) {
    var rows = document.getElementById(tblId).rows
    for (var i = 0; i < rows.length; i++) {
        var newCell = rows[i].insertCell(-1);
        if (i == 0) {
            newCell.innerHTML = "new col";
        }
        newCell.setAttribute('contenteditable', '');
    }
}

//表格删除列
function deleteColumn(tblId) {
    var index = prompt("请输入列号,默认为最后一列", "-1");
    if (index) {
        var allRows = document.getElementById(tblId).rows;
        for (var i = 0; i < allRows.length; i++) {
            if (allRows[i].cells.length > 1) {
                allRows[i].deleteCell(index);
            }
        }
    }
}

//表格增加行
function addRow(tblId) {
    var table = document.getElementById(tblId);
    if (table.rows.length > 0) {
        var col_count = table.rows[0].cells.length;
        var new_row = table.insertRow(-1);
        for (var i = 0; i < col_count; i++) {
            var new_cell = new_row.insertCell(-1);
            new_cell.setAttribute("contenteditable", "");
            new_cell.innerHTML = "new row";
        }
    }
    else {
        table.innerHTML = '<tr><td>#</td><td>列名</td></tr>';
    }
}

//表格复制一行
function cloneRow(tblId) {
    var index = prompt("请输入行号,默认为第一行", "1");
    if (index) {
        var table = document.getElementById(tblId);
        if (table.rows.length > 0) {
            var src_cells = table.rows[index].cells;
            var new_row = table.insertRow(-1);
            for (var i = 0; i < src_cells.length; i++) {
                var new_cell = new_row.insertCell(-1);
                new_cell.setAttribute("contenteditable", "");
                new_cell.innerHTML = src_cells[i].innerHTML;
            }
            new_row.cells[0].innerHTML = table.rows.length - 1;
        }
        else {
            alert("表格是空的,没办法复制行!");
        }
    }
}

//表格删除行
function deleteRow(tblId) {
    var index = prompt("请输入行号,默认为最后一行", "-1");
    if (index) {
        var table = document.getElementById(tblId);
        table.deleteRow(index);
    }
}