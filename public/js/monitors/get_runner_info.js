/**
 * Created with JetBrains RubyMine.
 * User: wuhuizuo
 * Date: 14-1-7
 * Time: 下午3:37
 * To change this template use File | Settings | File Templates.
 */

function update_process(uri, p_fieldset) {
    $.ajax({
        url: '/monitors/running_progress',
        type: 'get',
        data: {uri: uri},
        dataType: 'json',
        success: function (json) {
            set_process(json, p_fieldset);
        }
    });
}

function update_process_and_log(uri) {
    $.ajax({
        url: '/monitors/running_deta_json',
        type: 'get',
        data: {uri: uri},
        dataType: 'json',
        success: function (json) {
            set_process_and_log(json);
        }
    });
}

function set_process(progress, parent) {
    var progress_header_msg = 'total:' + progress['total'] + ',' + 'done:' + progress['done'];
    progress_header_msg += '(pass:' + progress['pass'] +'|fail:' + progress['fail'] + '),';
    var tc_reg = new RegExp("^<.+?>.+?#\\[.+?\\].+");
    if (tc_reg.test(progress['run_test'])) {
        var split_ret = progress['run_test'].split(/[<>#\[\]]/);
        var tc_href = "/tests/case?type=" + split_ret[1] + "&suite=" + split_ret[2] + "&id=" + split_ret[4] + "&opr=view";
        progress_header_msg += 'running:<a href="' + tc_href + '" target="_blank">' + progress['run_test'] + '</a>';
    } else {
        progress_header_msg += 'running:' + progress['run_test'];
    }
    $('#progress-header', parent).html(progress_header_msg);
    var done_percent = progress['done'] * 100.0 / progress['total'] + '%';
    var pass_percent = progress['pass'] * 100.0 / progress['total'] + '%';
    var fail_percent = progress['fail'] * 100.0 / progress['total'] + '%';
    $('#progress-bar-pass', parent).css('width', pass_percent);
    $('#progress-bar-pass', parent).html('pass:' + progress['pass']);
    $('#progress-bar-fail', parent).css('width', fail_percent);
    $('#progress-bar-fail', parent).html('fail:' + progress['fail']);
}

function set_process_and_log(json) {
    ace.edit("log").insert(json['log']);
    set_process(json['progress'], $('fieldset')[0]);
}

function init_log() {
    var log = ace.edit("log");
    log.setTheme("ace/theme/monokai");
    log.getSession().setMode("ace/mode/zwh");
    log.setReadOnly(true);
}



