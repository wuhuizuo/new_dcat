/**
 * Created with JetBrains RubyMine.
 * User: wuhuizuo
 * Date: 14-1-3
 * Time: 下午3:07
 * To change this template use File | Settings | File Templates.
 */

function set_glob_edit_dialog(title, data, opr) {
    var dialog_str = 'div#glob_edit_dialog';
    $(dialog_str.concat(' #glob_label')).html(title);
    for (var key in data) {
        $(dialog_str.concat(' [name=', key, ']')).val(data[key]);
    }
    $('div#glob_edit_dialog [name]').prop('readonly', opr == 'del');
    $('div#glob_edit_dialog [name=opr]').val(opr);
}

function parse_glob_edit_data(btn) {
    var cells = $('td', $(btn).closest('tr'));
    return {
        func: $(cells[0]).text(),
        old_func: $(cells[0]).text(),
        func_param: $(cells[1]).text(),
        old_func_param: $(cells[1]).text(),
        func_class: $(cells[2]).text(),
        old_func_class: $(cells[2]).text()
    };
}

$(document).ready(function () {
    $('.btn.btn-primary.submit-trigger').click(function () {
        $(this).parent().prev('.modal-body').find('form').submit();
    });
    $('.addModelBtn').on('click', function (event) {
        event.preventDefault();
        set_glob_edit_dialog('添加 全局关键字', parse_glob_edit_data(this), 'add');
    });
    $('.editModelBtn').on('click', function (event) {
        event.preventDefault();
        set_glob_edit_dialog('编辑 全局关键字', parse_glob_edit_data(this), 'edit');
    });
    $('.delModelBtn').on('click', function (event) {
        event.preventDefault();
        set_glob_edit_dialog('删除 全局关键字', parse_glob_edit_data(this), 'del');
    });
});
