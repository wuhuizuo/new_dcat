/**
 * Created with JetBrains RubyMine.
 * User: wuhuizuo
 * Date: 14-1-3
 * Time: 下午3:12
 * To change this template use File | Settings | File Templates.
 */

function update_object_func_param(klass, func, active_param) {
    $.ajax({
        url: '/tc_snipper/config/object_func_param',
        type: 'get',
        data: {  klass: klass, func: func },
        success: function (data) {
            $('select#object_func_param').html(data);
            if (typeof(active_param) == 'string' && active_param.length > 0) {
                $('select#object_func_param').val(active_param);
            }
        }
    });
}

function update_object_func(klass, active_func) {
    $.ajax({
        url: '/tc_snipper/config/object_func',
        type: 'get',
        data: {klass: klass },
        success: function (data) {
            $('select#object_func').html(data);
            if (typeof(active_func) == 'string' && active_func.length > 0) {
                $('select#object_func option').each(function () {
                    this.selected = ($(this).text() == active_func);
                });
            }
            update_object_func_param(klass, $('select#object_func option:selected').text(), null);
        }
    });
}

function update_object_class(active_class) {
    $.ajax({
        url: '/tc_snipper/config/object_class',
        type: 'get',
        success: function (data) {
            $('select#object_class').html(data);
            if (typeof(active_class) == 'string' && active_class.length > 0) {
                $('select#object_class').val(active_class);
            }
            update_object_func($('select#object_class').val(), null);
        }
    });
}

//依据操作历史设定页面输入值.
function update_all_by_history(data) {
    switch (data['opr'].split('_object_')[1]) {
        case 'class':
            update_object_class(data['klass']);
            break;
        case 'func':
            update_object_func($('select#object_class').val(), data['func']);
            break;
        case 'param':
            update_object_func_param(data['klass'], data['func'], data['param']);
            break;
        default:
            break;
    }
}

function display_object_edit_dialog_input(opr) {
    var opr_and_part = opr.split('_object_');
    var dialog_str = 'div#object_edit_dialog';
    $(dialog_str.concat(' input[name=opr]')).val(opr);
    var readonly = (opr_and_part[0] == 'del');
    switch (opr_and_part[1]) {
        case 'class':
            $(dialog_str.concat(' div#div_class')).show();
            $(dialog_str.concat(' [name=klass]')).prop('readonly', readonly);
            $(dialog_str.concat(' div#div_func')).hide();
            $(dialog_str.concat(' div#div_param')).hide();
            break;
        case 'func':
            $(dialog_str.concat(' div#div_class')).show();
            $(dialog_str.concat(' [name=klass]')).prop('readonly', true);
            $(dialog_str.concat(' div#div_func')).show();
            $(dialog_str.concat(' [name=func]')).prop('readonly', readonly);
            $(dialog_str.concat(' [name=code]')).prop('readonly', readonly);
            $(dialog_str.concat(' div#div_param')).hide();
            break;
        case 'param':
            $(dialog_str.concat(' div#div_class')).show();
            $(dialog_str.concat(' [name=klass]')).prop('readonly', true);
            $(dialog_str.concat(' div#div_func')).show();
            $(dialog_str.concat(' [name=func]')).prop('readonly', true);
            $(dialog_str.concat(' [name=code]')).prop('readonly', true);
            $(dialog_str.concat(' div#div_param')).show();
            $(dialog_str.concat(' [name=param]')).prop('readonly', readonly);
            break;
        default:
            break;
    }

}

function set_object_edit_dialog(data, opr) {
    var dialog_str = 'div#object_edit_dialog';
    var ns = ['klass', 'func', 'param', 'code'];
    for (var n in ns) $(dialog_str.concat(' [name=', ns[n], ']')).val(data[ ns[n]]);
    display_object_edit_dialog_input(opr);
}

function get_data_from_object_dialog() {
    var dialog_str = 'div#object_edit_dialog';
    return {
        klass: $(dialog_str.concat(' [name=klass]')).val(),
        func: $(dialog_str.concat(' [name=func]')).val(),
        param: $(dialog_str.concat(' [name=param]')).val(),
        code: $(dialog_str.concat(' [name=code]')).val(),
        opr: $(dialog_str.concat(' [name=opr]')).val()
    }
}

$(document).ready(function () {
    update_object_class(null);
    $('select#object_class').change(function () {
        update_object_func(this.value, null);
    });
    $('select#object_func').change(function () {
        var klass = $('select#object_class').val();
        update_object_func_param(klass, $(this).find("option:selected").text(), null);
    });

    $('button#add_object_class').click(function () {
        var klass = $('select#object_class').val();
        set_object_edit_dialog({klass: klass}, 'add_object_class')
    });
    $('button#del_object_class').click(function () {
        var klass = $('select#object_class').val();
        set_object_edit_dialog({klass: klass}, 'del_object_class')
    });
    $('button#add_object_func').click(function () {
        var klass = $('select#object_class').val();
        var func = $('select#object_func option:selected').text();
        var code = $('select#object_func').val();
        set_object_edit_dialog({klass: klass, func: func, code: code}, 'add_object_func')
    });
    $('button#del_object_func').click(function () {
        var klass = $('select#object_class').val();
        var func = $('select#object_func option:selected').text();
        var code = $('select#object_func').val();
        set_object_edit_dialog({klass: klass, func: func, code: code}, 'del_object_func')
    });
    $('button#add_object_param').click(function () {
        var klass = $('select#object_class').val();
        var func = $('select#object_func option:selected').text();
        var code = $('select#object_func').val();
        var param = $('select#object_func_param').val();
        set_object_edit_dialog({klass: klass, func: func, param: param, code: code}, 'add_object_param')
    });
    $('button#del_object_param').click(function () {
        var klass = $('select#object_class').val();
        var func = $('select#object_func option:selected').text();
        var code = $('select#object_func').val();
        var param = $('select#object_func_param').val();
        set_object_edit_dialog({klass: klass, func: func, param: param, code: code}, 'del_object_param')
    });

    $('.btn.btn-primary.submit-trigger').click(function () {
        var form_data = get_data_from_object_dialog();
        $.ajax({
            type: 'post',
            url: '/tc_snipper/config/object',
            data: form_data,
            success: function () {
                update_all_by_history(form_data);
            }
        });
    });
});