/**
 * Created with JetBrains RubyMine.
 * User: wuhuizuo
 * Date: 14-1-7
 * Time: 上午11:44
 * To change this template use File | Settings | File Templates.
 */
function get_tcs(version) {
    $.ajax({
        url: "/tests/tc_tree",
        type: "get",
        dataType: 'json',
        data: {ac_version: version},
        success: function (json) {
            $('#jstree_debug_div').jstree({'plugins': ['checkbox'], 'core': {
                'data': json.map(function (pool) {
                    return { 
                        text: pool.type, 
                        icon: 'glyphicon glyphicon-home',
                        a_attr: {href: '/tests/index?&type=' + pool.type},
                        children: parse_tc_tree_data(pool.type, pool.data) 
                    };
                })}
            });
        }
    });
}

function get_device_cfg(cfg_name) {
    $.ajax({
        url: "/cfgs/testdevice",
        type: "get",
        data: {cfg_name: cfg_name},
        success: function (data) {
            $("#active_cfg").html(data);
            $("select#ac_version").prop('disabled', true);
            get_tcs($("select#ac_version").val());
        }
    });
}
function get_device_cfg_names() {
    $.ajax({
        url: '/cfgs/device_cfg_names',
        type: 'get',
        dataType: 'json',
        success: function (json) {
            $('select#cfg_name').html('');
            $.each(json, function (i, cfg_name) {
                $('select#cfg_name').append('<option>' + cfg_name + '</option>');
            });
            get_device_cfg($('select#cfg_name').val());
        }
    });
}

function get_run_server_status() {
    var statu_table = $('table#drb_status');
    var drb_select = $('select#drb_uri');
    statu_table.html('<tr><th>执行机地址</th><th>执行机状态</th></tr>');
    drb_select.html('');
    $.ajax({
        url: '/cfgs/json_list_drblist',
        type: 'get',
        dataType: 'json',
        success: function (json) {
            $.each(json, function (i, uri) {
                $.ajax({
                    url: '/tools/test_dr',
                    type: 'get',
                    async: false,
                    data: {uri: uri},
                    success: function (data) {
                        statu_table.append('<tr><td>' + uri + '</td><td>' + data + '</td></tr>');
                        if (data == 'online') {
                            drb_select.append('<option>' + uri + '</option>')
                        }
                    }
                });
            });
            //清除加载中的状态提示.
            var load_tip = $('span#drb_loading')
            load_tip.removeClass('glyphicon-time');
            load_tip.addClass('glyphicon-refresh');
            load_tip.html('<a href="#" onclick="get_run_server_status()">刷新</a>')
            if (drb_select.val() != null)  $('button[type=submit]').prop('disabled', false);
        }
    });
}
function parse_tc_tree_data(tc_type, suite_hash) {
    var suitesNames = Object.keys(suite_hash).sort();
    return suitesNames.map(function (s) {
        return {
            text: s,
            a_attr: {href: '/tests/suite?&name=' + s + '&type=' + tc_type},
            icon: 'glyphicon glyphicon-folder-open',            
            children: suite_hash[s]['case'].map(function (tc) {
                return {
                    text: tc.case_id + '#' + tc.case_name,
                    icon: 'glyphicon glyphicon-credit-card',
                    a_attr: {href: '/tests/case?opr=view&suite=' + s + '&id=' + tc.case_id + '&type=' + tc_type},
                    data: tc.case_id
                };
            })
        };
    });
}
/*
 * 返回的样式是这种格式: ｛ query: {ActionSearch: [1,2,3]}, FluxSearch: [1,2,3]} }
**/
function get_selected_tcs(jstree_id) {
    var tree_json = $('#' + jstree_id).jstree().get_json('#', {}, false);
    var select_tcs = {};
    // - 遍历每一个测试套件
    tree_json.forEach(function (type) {
        t = type.text;
        select_tcs[t] = {};
        type.children.forEach(function (suite) {
            //如果有用例则遍历其中每一个用例选择状态
            var tcs = suite.children.filter(function (tc) {
                return tc.state.selected;
            }).map(function (tc) {
                    return tc.data;
                });
            if (tcs.length > 0) select_tcs[t][suite.text] = tcs;
            //否则略过这个套件
        });
    });
    return select_tcs;
}

function add_task(btn) {
    //获取用例
    var tcs = get_selected_tcs('jstree_debug_div');
    //获取测试设备配置和执行机配置
    var cfg = {};
    $(btn).parent().find('[name]').toArray().forEach(function (input) {
        cfg[input.name] = input.value;
    });
    $.ajax({
        type: "post",
        data: {tcs: tcs, cfg: cfg},        
        success: function (data) {
            var win = window.open(data, '_blank');
            win.focus();
        }
    });
}