# coding: utf-8
require 'drb'
require 'zip'
require 'find'
require 'tempfile'
require 'fileutils'

DCWebServer::Reports.helpers do
  # *描述*:保存内容到文件
  def save_content(content, dst_path)
    FileUtils.mkdir_p File.dirname(dst_path)
    File.open(dst_path, 'wb') do |f|
      f.write content
    end
  end

  # *描述*:和指定uri的服务器同步报表(下行)
  def sync_with_uri(uri, save_dir)
    dst_path = uri.split(%r{[:/]+})[1..-1].join('_')
    dr = DRbObject.new_with_uri uri
    dr.get_report_file_list.each do |f, f_size|
      dst_file = f.sub(%r{^\.\./result/}, "#{save_dir}/#{dst_path}/")
      next if File.exist?(dst_file) && File.size(dst_file) == f_size
      save_content(dr.get_file(f), dst_file)
    end
  end

  # *描述*:打包下载指定目录
  def download_dir_compress(path, remove_after = true)
    if File.exists?(path)
      path.sub!(%r{/$}, '')
      sample_tempfile = Tempfile.new("#{File.basename(path)}")
      zip_dir = sample_tempfile.path + '.zip'
      Zip::File.open(zip_dir, Zip::File::CREATE) do |zipfile|
        Dir["#{path}/**/**"].each { |f| zipfile.add(f.sub(path+'/', ''), f) }
      end
      send_file zip_dir, :type => 'application/zip', :disposition => 'attachment', :filename => "#{File.basename(zip_dir)}"
      File.delete(zip_dir) if remove_after
    end
  end

  # *描述*:获取素有执行机的报表列表
  # @param:server_pool[Array]:执行服务器列表
  def get_report_list(server_pool)
    #result = get_server_status(server_pool)
    report_results = []
    #result.each do |server, st|
    server_pool.each do |server|
      report_dir = 'result/' + server.split(%r{[:/]+})[1..-1].join('_')
      report_htmls = Dir.glob("#{report_dir}/**/*.html")
      report_htmls = report_htmls.map { |p| p.sub(/^public\//, "/") }.sort
      report_results << {:uri => server, :st => 'online', :reports => report_htmls}
    end
    report_results.sort! { |a, b| a[:uri] <=> b[:uri] }
  end
end
