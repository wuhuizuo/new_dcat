require 'yaml'

src_tc = YAML.load_file '1.yaml'

(2..8).each do |d|
  dst_yaml = "#{d}.yaml"
  dst_tc = YAML.load_file dst_yaml
  dst_tc['record_datas'] = src_tc['record_datas']
  dst_tc['case_checks'] = src_tc['case_checks']
  File.open(dst_yaml, 'w'){|f| f.write dst_tc.to_yaml }
end
