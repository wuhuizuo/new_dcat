DCWebServer::Tests.controllers do
  get '/case_att' do
    tc = get_tc(params[:type], params[:suite], params[:id])
    att_name = params[:att_name]
    att = tc['data'][att_name]
    erb :'func_case/att_item_detail', :layout => false, :locals => {:att => att, :name => att_name}
  end

  post '/case_att' do
    puts params
    opr_att params[:opr], params[:type], params[:suite], params[:id], params
    'success'
  end

  get '/case_att_list' do
    tc = get_tc(params[:type], params[:suite], params[:id])
    erb :'func_case/att_list', :layout => false, :locals => {:data => tc['data']}
  end
end