require 'drb'
require 'yaml'
DCWebServer::Tests.controllers do
	post '/case_debug' do		 
	  args = YAML.load params.to_yaml #克隆解决问题
	  dr = DRbObject.new_with_uri(params['drb'])	  
	  dr.debug(args['code'], args['cfg'])
	end
end