DCWebServer::Tests.controllers do
  get '/index' do
    title = {'func' => '逻辑功能测试用例套 列表', 'query' => '数据查询测试用例套 列表'}[params[:type]]
    suites = get_case_pool(params[:type]) || {}   
    erb :index, :locals => {:type => params[:type], :title => title, :suites => suites}
  end

  get '/suite' do
    suite = get_suite_index params[:type], params[:name]
    tcs = suite[:case] || [] 
    tcs.sort!{|a,b| a['case_id'].to_i <=> b['case_id'].to_i } if tcs.length > 1
    erb :suite, :locals => {:type => params[:type], :suite => params[:name], :tcs => tcs}
  end

  post '/suite' do
    begin
      case params[:opr]
        when 'add'
          add_suite(params[:type], params[:name], params[:desc])
        when 'del'
          del_suite(params[:type], params[:name])
        when 'edit'
          edit_suite(params[:type], params[:old_name], params[:name], params[:old_desc], params[:desc])
        else
          #nothing
      end
      redirect "/tests/index?type=#{params[:type]}"
    rescue Exception => e
      e.message
    end
  end


  get '/case' do
    type, suite_name, case_id, opr = params[:type], params[:suite], params[:id], params[:opr]
    case type
      when 'func', 'query'
        op_tc(type, suite_name, case_id, opr)
      else
        halt "不支持的用例类型:#{type}"
    end
  end
end