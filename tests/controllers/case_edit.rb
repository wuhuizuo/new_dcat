DCWebServer::Tests.controllers do
  post '/case' do
    type, suite, id = params[:tc_type], params[:suite_name], params[:case_id]
    tc_file = get_tc_file(type, suite, id)
    tc = load_tc_from_file(tc_file) || {}
    case type
      when 'func'
        tc = edit_func_tc(tc, params)
      when 'query'
        tc = edit_query_tc(tc, params)
      else
        halt "不支持的用例类型#{type}"
    end
    save_tc_to_file(tc, tc_file, false) #不备份了。
    'SUCCESS'
  end
end