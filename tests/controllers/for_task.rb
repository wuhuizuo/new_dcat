require 'json'
DCWebServer::Tests.controllers do
  get '/tc_tree' do
    pool = [
        {'type' => 'query', 'data' => get_case_pool('query') },
        {'type' => 'func', 'data' => get_case_pool('func') }
    ]
    pool.each do |e_p|
    	e_p['data'].each do |key, suite|
    	  suite[:case].select do |tc| 
			id = tc['case_id']
			id =~ /^\d+$/ or (id =~ /\d+_.+/ && id.split('_', 2)[1] == params[:ac_version])
    	  end
    	end
    end
    pool.to_json
  end
end