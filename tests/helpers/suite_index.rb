require 'yaml'
DCWebServer::Tests.helpers do
	#*描述*:获取指定套件的索引信息。
	# @param	type[String] 测试套件类型 `func|query`
	# @param  suite_name[String]:用例套件名
	# @param  update_hash[Hash]:需要更新的hash键值项
  def update_suite_index(type, suite_name, update_hash)
    index_file = File.join(get_suite_dir(type, suite_name), '.index.yaml')    
    suite = (File.exist?(index_file) ? YAML.load_file(index_file) : {:case => []})
    suite.merge! update_hash
    File.open(index_file, 'w') { |f| f.write suite.to_yaml }    
  end

  #*描述*:获取指定套件的索引信息。
  # @param	type[String] 测试套件类型 `func|query`
	# @param  suite_name[String]:用例套件名
	# @return [Hash]
  def get_suite_index(type, suite_name)
  	suite_dir = get_suite_dir(type, suite_name)
  	raise "不存在测试套件:#{suite_name} !" unless File.exist?(suite_dir)
  	index_file = File.join(suite_dir, '.index.yaml')    
  	suite = File.exist?(index_file) ? YAML.load_file(index_file) : nil
    suite[:case].sort!{|a,b| a['case_id'].to_i <=> b['case_id'].to_i } if suite and suite[:case].length > 1
    suite
  rescue Exception => e
  	raise "索引文件损坏或者加载解析失败!"
  end

  def update_suite_index_case_name(type, suite_name, case_id, new_case_name)
    tcs = get_suite_index(type,suite_name)[:case]
    tcs.find{|tc| tc['case_id'] == case_id}['case_name'] = new_case_name
    update_suite_index(type, suite_name, :case => tcs)
  end
end