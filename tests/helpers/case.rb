require 'yaml'
require 'fileutils'
require 'csv'

DCWebServer::Tests.helpers do
  def add_table_tpl(table_type, table_date)
    table_hash = TableTpl[table_type]
    {
        'table_name' => table_type,
        'table_date' => table_date,
        'table_datas' => [table_hash]
    }
  end

  def load_tc_from_file(file)
    File.exists?(file) ? YAML.load_file(file) : nil
  end

  def save_tc_to_file(tc, file, need_backup = true)
    FileUtils.cp file, file + Time.now.strftime('%Y%m%d_%H.%M.%S') if need_backup
    File.open(file, 'w') { |f| f.write tc.to_yaml }
  end


  # *描述*:编辑更新测试用例
  # @param  dst_tc[Hash]: 待更新的测试用例
  # @param  params[Hash]: 更新请求选项
  # @return [Hash]:更新后的dst_tc
  def edit_query_tc(dst_tc, params)
    case params[:edit_part]
      when 'case_data'
        dst_tc['record_datas'] = parse_case_data(params[:record_datas])
      when 'summary'
        dst_tc['web_name'] = params[:case_webname]
        #修改了用例名称时,需要更新测试套件索引文件
        unless dst_tc['case_name'] == params[:case_name]
          dst_tc['case_name'] = params[:case_name]
          update_suite_index_case_name('query', params[:suite_name], params[:case_id], params[:case_name])          
        end
      when 'case_config'
        dst_tc['case_configs'] = parse_case_config(params[:case_configs])
      when 'case_check'
        dst_tc['case_checks'] = parse_case_check(params[:case_checks])
        #处理查询模块的用例结构 ---start
        check_cols = dst_tc['case_checks'][0].keys
        if check_cols.include?('hope_datas') and check_cols.include?('check_datas')
          dst_tc['case_checks'] = parse_case_check_search(dst_tc['case_checks'], dst_tc['record_datas'])
        end
      #处理查询模块的用例结构 ---end
      when 'init_check'
        init_check = [{'检查项名(请修改)' => '期望值(请修改)'}]
        dst_tc['case_checks'] = init_check unless dst_tc['case_checks'] and dst_tc['case_checks'].length > 0
      when 'init_config'
        init_conifg = {'配置项名(请修改)' => '配置值(请修改)'}
        dst_tc['case_configs'] = init_conifg unless dst_tc['case_configs'] and dst_tc['case_configs'].length > 0
      when 'add_case_data'
        add_table_name = params[:table_name]
        add_table_date = Time.now.strftime('%Y%m%d')
        dst_tc['record_datas'] << add_table_tpl(add_table_name, add_table_date)
    end
    dst_tc
  end

  # *描述*:编辑更新测试用例
  # @param  dst_tc[Hash]: 待更新的测试用例
  # @param  params[Hash]: 更新请求选项
  # @return [Hash]:更新后的dst_tc
  def edit_func_tc(dst_tc, params)
    #修改了用例名称时,需要更新测试套件索引文件
    unless dst_tc['case_name'] == params[:case_name]
      dst_tc['case_name'] = params[:case_name]          
      update_suite_index_case_name('func', params[:suite_name], params[:case_id], params[:case_name])          
    end
    dst_tc['steps'] = params[:steps]
    dst_tc['code'] = params[:code]
    dst_tc
  end

  def get_tc_file(type, suite, id, fake_opr = false)
    suite_dir = get_suite_dir(type, suite)
    assert_tc_type(type)
    file = File.join(suite_dir, "#{id}.yaml")      
    raise "获取测试用例文件失败,不存在文件:#{file}" unless File.exists?(file) or fake_opr
    file
  end

  # *描述*:在测试套中操作用例，包括 查看、编辑、删除、克隆
  def op_tc(type, suite, case_id, opr)
    error_msg = %{Sorry! 没有这个用例...<a href=“/tests/suite?name=#{suite}”>返回</a>}
    case opr
      when 'edit', 'view'
        tc = get_tc(type, suite, case_id)
        return error_msg unless tc
        tc_params = {
            :type => type,
            :suite => suite,
            :case_id => case_id,
            :tc => tc,
            :opr => opr}
        assert_tc_type(type)
        tc_params[:case_ids] = get_suite_index(type, suite)[:case].map{|e| e['case_id'] }
        erb :"#{type}_case", :locals => tc_params          
      when 'clone', 'del'
        send("#{opr}_tc", type, suite, case_id)
        redirect "/tests/suite?type=#{type}&name=#{suite}"
      when 'add'
        add_empty_tc(type, suite)
        redirect "/tests/suite?type=#{type}&name=#{suite}"
      else
        raise "不支持的用例操作:#{opr}!"
    end
  end

  # 从csv字符串或者文件解析成数组
  # @param [String] type csv数据类型, 'csv_string' or 'csv_file'
  # @param [String]  value csv数据值,当 `type`为 csv_string时,表示csv内字符串值,当为csv_file时,表示csv文件的路径.
  #
  # @return [Array<Hash>, Array<Array>]
  #   unless csv_opt[:hash_col_name] is true, then the hash key is col1,col2,col3......coln
  def trans_csv_to_array(type, value)
    array_2d = case type
                 when 'csv_string'
                   CSV.parse value
                 when 'csv_file'
                   CSV.read File.join(DCWebServer::Tests::FUNC_CASE_DIR, '..', '..', 'public', value)
                 else
                   raise ArgumentError, "数据类型#{type}不支持解析为csv数据!"
               end
    if array_2d.length > 1
      keys = array_2d[0]
      array_2d[1..-1].map { |row| Hash[keys.zip row] }
    else
      nil
    end
  end


  def get_tc(type, suite, id)
    load_tc_from_file get_tc_file(type, suite, id)
  end

  def del_tc(type, suite, id)
    File.delete get_tc_file(type, suite, id)
    #更新套件索引文件
    tcs = get_suite_index(type, suite)[:case]
    tcs.delete_if{|tc| tc['case_id'] == id}
    update_suite_index(type, suite, :case => tcs)
  end

  #克隆测试用例
  # @param [String] type 测试用例类型"func|query"
  # @param [String] suite 测试套件名字
  # @param [String] id 用例id
  def clone_tc(type, suite, id)
    assert_tc_type type
    tcs = get_suite_index(type, suite)[:case]
    src_file = get_tc_file(type, suite, id)
    max_id_tc = tcs.max { |a, b| a['case_id'].to_i <=> b['case_id'].to_i }
    new_id = max_id_tc['case_id'].to_i + 1
    dst_file = get_tc_file(type, suite, new_id, true)
    FileUtils.copy_file(src_file, dst_file)
    #更新套件索引文件
    tcs << {'case_id' => new_id.to_s, 'case_name' => max_id_tc['case_name']}
    update_suite_index(type, suite, :case => tcs)
  end

  # @return [String] 添加新用例的id
  def add_empty_tc(type, suite)
    assert_tc_type type
    init_case = {
      'query' => {
        'record_datas' => [add_table_tpl('A', 'TODAY')],
        'case_checks' => [{'检查点名' => '期望检查点值'}],
        'web_name' => '这里写web页面名称(那个菜单名)',
        'case_name' => "新添加的查询准确性用例(#{Time.now})",
        'case_configs' => {'页面设置项(页面上度配置名字是啥这里写啥)' => '配置值'}
      },
      'func' => {
        'case_name' => "新添加的功能用例(#{Time.now})",
        'steps' => '//你该在这里写点啥.',
        'code' => '#你还没有编写过这个用例呢。。'
      }
    }[type]
    tcs = get_suite_index(type, suite)[:case]
    max_id_tc = tcs.max { |a, b| a['case_id'].to_i <=> b['case_id'].to_i }
    new_id = max_id_tc['case_id'].to_i + 1    
    dst_file = get_tc_file(type, suite, new_id, true)
    save_tc_to_file(init_case, dst_file, false)
    #更新套件索引文件
    tcs << {'case_id' => new_id.to_s, 'case_name' => init_case['case_name']}
    update_suite_index(type, suite, :case => tcs)
  end



  # csv数据内容编辑更新
  def opr_att_update_csv_data(type, suite, case_id, att)
    dst_tc = get_tc type, suite, case_id
    # 解析json字符串成 ruby二维数组
    if  dst_tc['data'][att[:att_name]][:type] =~ /csv/
      csv_string = array2d_to_csv JSON.parse(att[:att_value]).map(&:values)
    end

    case dst_tc['data'][att[:att_name]][:type]
      when 'csv_file'
        file = File.join(get_public_dir, dst_tc['data'][att[:att_name]][:value])
        File.open(file, 'wb') { |f| f.write csv_string }
      when 'csv_string'
        dst_tc['data'][att[:att_name]][:value] = csv_string
      else
        raise ArgumentError, "数据附件类型为:#{dst_tc['data'][att[:att_name]][:type]},不支持csv格式编辑!"
    end
    tc_file = get_tc_file type, suite, case_id
    save_tc_to_file(dst_tc, tc_file, false) #不备份了。
  end

# 字符串内容编辑更新
  def opr_att_update_string(type, suite, case_id, att)
    dst_tc = get_tc type, suite, case_id
    dst_tc['data'][att[:att_name]][:value] = att[:att_value]
    tc_file = get_tc_file type, suite, case_id
    save_tc_to_file(dst_tc, tc_file, false) #不备份了。
  end

# 文件类型上传更新
  def opr_att_update_file(type, suite, case_id, att)
    dst_tc = get_tc type, suite, case_id
    #保存编辑文本
    save_target = save_att_file(suite, case_id, att)
    dst_tc['data'][att[:att_name]][:value] = save_target if save_target
    tc_file = get_tc_file type, suite, case_id
    save_tc_to_file(dst_tc, tc_file, false) #不备份了。
  end

# 数据附件删除
  def opr_att_del(type, suite, case_id, att)
    dst_tc = get_tc type, suite, case_id
    tc_file = get_tc_file type, suite, case_id
    if  dst_tc['data'][att[:att_name]][:type] =~ /file/
      att_file = File.join(get_public_dir, dst_tc['data'][att[:att_name]][:value])
      FileUtils.rm_rf(att_file) if File.exists? att_file
      dst_tc['data'].delete(att[:att_name])
      #当没有文件时,将对应的目录删除。
      FileUtils.rm_rf(File.dirname att_file) if dst_tc['data'].select { |_, att| att[:type] =~ /file/ }.size == 0
    else
      dst_tc['data'].delete(att[:att_name])
    end
    save_tc_to_file(dst_tc, tc_file, false) #不备份了。
  end

# 附件克隆
  def opr_att_clone(type, suite, case_id, att)
    dst_tc = get_tc type, suite, case_id
    clone_name = "#{att[:att_name]}_#{dst_tc['data'].keys.grep(/^#{att[:att_name]}/).length}"
    dst_tc['data'][clone_name] = dst_tc['data'][att[:att_name]]
    if dst_tc['data'][clone_name][:type] =~ /file/
      src_file = dst_tc['data'][clone_name][:value]
      sufix = src_file.match(%r{.+?(\.\w+)$})[1]
      clone_file = File.join(File.dirname(src_file), "#{clone_name}.#{sufix}")

      FileUtils.cp File.join(get_public_dir, src_file), File.join(get_public_dir, clone_file)
      dst_tc['data'][clone_name][:value] = clone_file
    end
    tc_file = get_tc_file type, suite, case_id
    save_tc_to_file(dst_tc, tc_file, false) #不备份了。
  end

# 增加附件数据
# @param [Hash] att 附件属性,例如: {:att_name => 'xxx',:att_type => 'csv_string'}
  def opr_att_add(type, suite, case_id, att)
    dst_tc = get_tc type, suite, case_id
    dst_tc['data'] ||= {}
    dst_tc['data'][att[:att_name]] = {:type => att[:att_type], :value => nil}
    if att[:att_type] =~ /csv/
      csv_data_tpl = array2d_to_csv [%w(样例姓名 样例值), %w(张三 175)]
      case att[:att_type]
        when 'csv_string'
          dst_tc['data'][att[:att_name]][:value] = csv_data_tpl
        when 'csv_file'
          csv_file_att = {:att_file_data => csv_data_tpl, :att_name => att[:att_name], :att_file_name => 'tpl.csv'}
          puts csv_file_att
          dst_tc['data'][att[:att_name]][:value] = save_att_file(suite, case_id, csv_file_att)
      end
    end
    if att[:att_type] == 'bin_file'
      csv_file_att = {:att_file_data => '', :att_name => att[:att_name], :att_file_name => 'xxx.dat'}
      save_att_file(suite, case_id, csv_file_att)
    end
    tc_file = get_tc_file type, suite, case_id
    save_tc_to_file(dst_tc, tc_file, false) #不备份了。
  end

# 附件操作接口函数
  def opr_att(opr, type, suite, case_id, att)
    raise ArgumentError, '没有指定附件名称!' unless att[:att_name]
    raise ArgumentError, '不支持的操作类型' unless respond_to?("opr_att_#{opr}")
    send("opr_att_#{opr}", type, suite, case_id, att)
  end

  def get_att_file_dir(suite, case_id)
    "/files/att_data/#{suite}/#{case_id}"
  end

  def get_public_dir
    File.join DCWebServer::Tests::FUNC_CASE_DIR, '..', '..', 'public'
  end

  def save_att_file(suite, case_id, att)
    if att[:att_file_name] and att[:att_file_data]
      file_dir = File.join(get_public_dir, get_att_file_dir(suite, case_id))
      file_name = "#{att[:att_name]}.#{att[:att_file_name].sub(/^.*\./, '')}"
      FileUtils.mkdir_p file_dir
      File.open(File.join(file_dir, file_name), 'wb') do |att_file|
        att_file.write att[:att_file_data]
      end
      File.join get_att_file_dir(suite, case_id), file_name
    end
  end

# 将二维数组转换成csv字符串
  def array2d_to_csv(array2d)
    CSV.generate { |csv| array2d.each { |row| csv << row } }
  end
end


