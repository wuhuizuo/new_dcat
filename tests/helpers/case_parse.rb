DCWebServer::Tests.helpers do
  def parse_case_data(case_data_json)
    case_datas = []
    temp_datas_array = JSON.parse(case_data_json.gsub('=&gt;', '=>'))
    temp_datas_array.each do |data|
      if data['table_datas'].length > 1
        table_data_hash = {}
        table_data_hash['table_name'] = data['table_name']
        table_data_hash['table_date'] = data['table_date']
        data_cols = data['table_datas'][0].sort.map { |c| c[1] }
        table_data_hash['table_datas'] = data['table_datas'][1..-1].map { |row|
          Hash[data_cols.zip(row.sort.map { |c| c[1] })]
        }
        case_datas << table_data_hash
      end
    end
    return case_datas
  end

  def parse_case_config(case_config_json)
    Hash[
        JSON.parse(case_config_json.gsub('=&gt;', '=>')).map { |row| [row['0'], row['1']] }
    ]
  end

  def parse_case_check(case_check_json)
    temp_check_array = JSON.parse(case_check_json.gsub('=&gt;', '=>'))
    if temp_check_array.nil? or temp_check_array.length == 0
      []
    else
      check_cols = temp_check_array[0].sort.map { |c| c[1] }
      temp_check_array[1..-1].map { |row|
        Hash[check_cols.zip(row.sort.map { |c| c[1] })]
      }
    end
  end

  # *描述*:专门解析查询模块的数据校验点度逻辑函数。
  # @param  [Array<Hash>] record_datas 用例数据结构
  # @param  [Array] check_array 待解析的用例结构
  def parse_case_check_search(check_array, record_datas)
    check_array.map do |check|
      puts check['hope_datas'].inspect
      check['check_datas'] = check['hope_datas'].split(%r{[\r|\n|;]+}).map do |h_d|
        t, d, r = h_d.split(%r{\s*:\s*}, 3)
        table = record_datas.find { |e| e['table_date'] == d && e['table_name'] == t }
        range = eval "[#{r}]"
        range = range.flatten.map { |e| e.respond_to?(:to_a) ? e.to_a : e }.flatten.uniq
        range.map { |i| table['table_datas'][i - 1] }
      end
      check
    end
  end

end