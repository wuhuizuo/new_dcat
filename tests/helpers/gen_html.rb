DCWebServer::Tests.helpers do
  def gen_tc_data_html(tc, readonly)
    erb :'query_case/part', :layout => false, :locals => {:part => 'data', :part_name => '数据准备'} do
      erb :'query_case/data', :layout => false, :locals => {:datas => tc['record_datas'], :readonly => readonly}
    end
  end

  #生成用例检查点的table的编辑的试图html
  def gen_tc_check_html(tc, readonly)
    erb :'query_case/part', :layout => false, :locals => {:part => 'check', :part_name => '用例校验'} do
      if tc['case_checks'] and tc['case_checks'].length > 0
        locals = {:check => tc['case_checks'], :readonly => readonly}
        erb :'query_case/check', :layout => false, :locals => locals
      else
        '什么都没有.. <a href="javascript:init_check()">初始化检查点</a> <hr /> ' unless readonly
      end
    end
  end

  def gen_tc_data_table_html(table_index, table_data, readonly)
    params = {
        :part => "data_table#{table_index}",
        :part_name => "数据表:#{table_data['table_name']}#{table_data['table_date']}"
    }
    erb :'query_case/part', :layout => false, :locals => params do
      table_params = {:data => table_data, :table_index => table_index, :readonly => readonly}
      erb :'query_case/data_table', :layout => false, :locals => table_params
    end
  end

  def gen_table_html(table_id, table_hash_array, readonly, fix_cols=false)
    erb :'query_case/table',
        :layout => false,
        :locals => {
            :fix_cols => fix_cols,
            :readonly => readonly,
            :table_id => table_id,
            :table_hash_array => table_hash_array
        }
  end

  #生成用例页面条件设置编辑度视图html
  def gen_tc_cfg_html(tc, readonly)
    erb :'query_case/part', :layout => false, :locals => {:part => 'cfg', :part_name => '用例配置'} do
      erb :'query_case/cfg',
          :layout => false,
          :locals => {:cfg => tc['case_configs'], :readonly => readonly}
    end
  end

  #*描述*:用例概要试图
  def gen_tc_summary_html(tc, readonly)
    erb :'query_case/part', :layout => false, :locals => {:part => 'summary', :part_name =>'用例概述'} do
      erb :'query_case/summary', :layout => false, :locals => {:tc => tc, :readonly => readonly}
    end
  end

  #生成用例操作链接
  def gen_tc_operator(case_id)
    %w{view edit del clone}.map { |o| %{<a href="/#{case_id}/#{o}" >#{o}</a>} }.join("\n")
  end

  def gen_tc_data_table_adder
    erb :'query_case/table_adder', :layout => false, :locals => {:tables => TableTpl.keys}
  end
end