require 'fileutils'

DCWebServer::Tests.helpers do
  def add_suite(type, suite_name, suite_desc)
    target_dir = get_suite_dir(type, suite_name)
    raise "#{type}测试套件已经存在:#{suite_name}" if File.exist?(target_dir)
    FileUtils.mkdir_p target_dir      
    #更新索引文件:
    update_suite_index(type, suite_name, :desc => suite_desc)
  end

  def del_suite(type, suite_name)
    target_dir = get_suite_dir(type, suite_name)
    raise "#{type}测试套不存在:#{suite_name}" unless File.exist?(target_dir)
    FileUtils.rm_rf target_dir
  end

  def edit_suite(type, suite_name, new_suite_name, suite_desc, new_suite_desc)
    src_dir = get_suite_dir(type, suite_name)
    dst_dir = get_suite_dir(type, new_suite_name)
    raise "#{type}测试套不存在:#{suite_name}" unless File.exist?(src_dir)
    FileUtils.move src_dir, dst_dir unless src_dir == dst_dir
    update_suite_index(type, suite_name, :desc => new_suite_desc) unless suite_desc == new_suite_desc
  end
end