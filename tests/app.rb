require 'yaml'

module DCWebServer
  class Tests < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    enable :sessions

    ##
    # Caching support.
    #
    # register Padrino::Cache
    # enable :caching
    #
    # You can customize caching store engines:
    #
    # set :cache, Padrino::Cache::Store::Memcache.new(::Memcached.new('127.0.0.1:11211', :exception_retry_limit => 1))
    # set :cache, Padrino::Cache::Store::Memcache.new(::Dalli::Client.new('127.0.0.1:11211', :exception_retry_limit => 1))
    # set :cache, Padrino::Cache::Store::Redis.new(::Redis.new(:host => '127.0.0.1', :port => 6379, :db => 0))
    # set :cache, Padrino::Cache::Store::Memory.new(50)
    # set :cache, Padrino::Cache::Store::File.new(Padrino.root('tmp', app_name.to_s, 'cache')) # default choice
    #

    ##
    # Application configuration options.
    #
    # set :raise_errors, true       # Raise exceptions (will stop application) (default for test)
    # set :dump_errors, true        # Exception backtraces are written to STDERR (default for production/development)
    # set :show_exceptions, true    # Shows a stack trace in browser (default for development)
    # set :logging, true            # Logging in STDOUT for development and file for production (default only for development)
    # set :public_folder, 'foo/bar' # Location for static assets (default root/public)
    # set :reload, false            # Reload application files (default in development)
    # set :default_builder, 'foo'   # Set a custom form builder (default 'StandardFormBuilder')
    # set :locale_path, 'bar'       # Set path for I18n translations (default your_apps_root_path/locale)
    # disable :sessions             # Disabled sessions by default (enable if needed)
    # disable :flash                # Disables sinatra-flash (enabled by default if Sinatra::Flash is defined)
    # layout  :my_layout            # Layout can be in views/layouts/foo.ext or views/foo.ext (default :application)
    #
    layout :'../../app/views/layouts/application'

    ##
    # You can configure for a specified environment like:
    #
    #   configure :development do
    #     set :foo, :bar
    #     disable :asset_stamp # no asset timestamping for dev
    #   end
    #

    ##
    # You can manage errors like:
    #
    #   error 404 do
    #     render 'errors/404'
    #   end
    #
    #   error 505 do
    #     render 'errors/505'
    #   end
    #

    QUERY_CASE_DIR = File.join(__dir__, 'query_data')
    FUNC_CASE_DIR = File.join(__dir__, 'func_data')

    def assert_tc_type(type)
      raise "不支持的用例类型:#{type} 的视图!" unless type =~ /^func|query$/
    end

    def get_suite_dir(type, suite_name)
      assert_tc_type type
      File.join eval("#{type}_CASE_DIR".upcase), suite_name
    end


    # *描述*: 更新用例列表
    # @param  type[String]: 什么类型的测试套件
    def get_case_pool(type)
      assert_tc_type type
      case_pool = {}
      type_dir = eval "#{type}_CASE_DIR".upcase
      Dir["#{type_dir}/*"].select { |f| File.directory?(f) }.each do |dir|
        suite_name = File.basename(dir)
        suite_index = get_suite_index(type, suite_name)
        if suite_index and suite_index[:case] and suite_index[:case].length > 1
          suite_index[:case].sort! { |a, b| a['case_id'] <=> b['case_id'] }
        end
        case_pool[suite_name] = suite_index
      end
      case_pool
    end
  end
end
