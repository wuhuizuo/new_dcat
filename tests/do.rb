require 'yaml'

def del_useless_key!(tc)
  keys = %w{case_id result config_id case_flag check_id insert_datas}
  keys.each { |k|
    if tc.key? k
      tc.delete k
      print '*'
    end
  }

  config_keys = %w{config_id id}
  config_keys.each do |k|
    tc['case_configs'].delete k if tc['case_configs'].key? k
    print '*'
  end
end

#换名字
def trans_config_name!(config)
  trans_map = {
      'object' => '查询对象-类型',
      'value' => '查询对象-值',
      'child' => '包含子组',
      'r_object' => '排除对象-类型',
      'r_obj' => '排除对象-类型',
      'r_value' => '排除对象-值',
      'date' => '日期',
      'action' => '动作',
      'im_num' => 'IM帐号'
  }
  trans_map.each do |v, k|
    unless config.key? k
      if config.key? v
        config[k] = config[v]
        config.delete v
        print '*'
      end
    end
  end
end

#多个组合
def trans_combine_date_range!(config)
  unless config.key? '日期范围'
    if config.key? 'date_from' and config.key? 'date_to'
      config['日期范围'] = "#{config['date_from']}~#{config['date_to']}"
      config.delete('date_from')
      config.delete('date_to')
      print '*'
    end
  end
end

def trans_combine_app!(config)
  unless config.key? '应用'
    if config.key? 'serv_type' and config.key? 'app_name'
      if config['app_name'] =~ /.+/
        config['应用'] = "#{config['serv_type']}:#{config['app_name']}"
      else
        config['应用'] = "#{config['serv_type']}"
      end
      config.delete('serv_type')
      config.delete('app_name')
      print '*'
    end
  end
end

def trans_combine_time_range!(config)
  unless  config.key? '时间'
    if config.key? 'time_from' and config.key? 'time_to'
      if config['time_to'] =~ /.+/
        config['时间'] = "#{config['time_from']}~#{config['time_to']}"
      else
        config['时间'] = "#{config['time_from']}"
      end
      config.delete('time_from')
      config.delete('time_to')
      print '*'
    end
  end
end

tcs_path = __dir__ + '/query_data'
files = Dir["#{tcs_path}/*/*.yaml"]

files.each do |f|
  begin
    tc = YAML.load_file f
    del_useless_key! tc
    trans_config_name! tc['case_configs']
    trans_combine_app! tc['case_configs']
    trans_combine_time_range! tc['case_configs']
    trans_combine_date_range! tc['case_configs']
    File.open(f, 'wb') { |f| f.write tc.to_yaml }
    print '.'
  rescue Exception => e
    puts f
    puts e.message
  end
end