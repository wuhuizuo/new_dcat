require 'drb'
DCWebServer::Monitors.helpers do
  def get_log(uri)
    @drs[uri] = DRbObject.new_with_uri(uri) unless @drs[uri]
    re = @drs[uri].get_log_buffer
    re
  end

  def get_progress(uri)
    @drs[uri] = DRbObject.new_with_uri(uri) unless @drs[uri]
    @drs[uri].progress
  end
end