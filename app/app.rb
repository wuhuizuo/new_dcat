module DCWebServer
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    set :raise_errors, true # Raise exceptions (will stop application) (default for test)
    set :dump_errors, true # Exception backtraces are written to STDERR (default for production/development)
    set :show_exceptions, true # Shows a stack trace in browser (default for development)
    set :logging, true
    set :reload, false
    enable :sessions
    # Logging in STDOUT for development and file for production (default only for development)
    #set :public_folder, File.join(APP_ROOT, 'public')
    # Location for static assets (default root/public)
    # Reload application files (default in development)
    # set :default_builder, 'foo' # Set a custom form builder (default 'StandardFormBuilder')
    #set :locale_path, 'bar' # Set path for I18n translations (default your_apps_root_path/locale)
    # Disabled sessions by default (enable if needed)
    # disable :flash
    # Disables sinatra-flash (enabled by default if Sinatra::Flash is defined)
    # layout :"layouts/default" # Layout can be in views/layouts/foo.ext or views/foo.ext (default :application)

    # Custom error management
    error(403) { @title = "Error 403"; render('errors/403', :layout => :error) }
    error(404) { @title = "Error 404"; render('errors/404', :layout => :error) }
    error(500) { @title = "Error 500"; render('errors/500', :layout => :error) }

    get '/' do
      erb :index
    end

    get '/about' do
      erb :about
    end

    get '/debug' do
      erb :debug
    end
    post '/debug' do
      'nothing'
    end
  end
end
