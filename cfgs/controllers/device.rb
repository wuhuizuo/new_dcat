require 'json'
DCWebServer::Cfgs.controllers do
  get '/testdevice' do
    cfg = @test_devices.find { |c| c[:cfg_name] == params[:cfg_name] }
    if cfg
      erb :testdevice_sub, :layout => false, :locals => {:cfg => cfg[:cfg], :stab => false}
    else
      "错误:不能存在配置#{params[:cfg_name]}, 或者读取配置失败!"
    end
  end

  get '/testdevices' do
    testdevices_view(@test_devices, DCWebServer::Cfgs::TESTDEVICE_CFG, '/cfgs/testdevices', false)
  end
  post '/testdevices' do
    testdevices_view(@test_devices, DCWebServer::Cfgs::TESTDEVICE_CFG, '/cfgs/testdevices', false, params)
  end

  get '/stab_testdevices' do
    testdevices_view(@stab_test_devices, DCWebServer::Cfgs::STAB_TESTDEVICE_CFG, '/cfgs/stab_testdevices', true)
  end
  post '/stab_testdevices' do
    testdevices_view(@stab_test_devices, DCWebServer::Cfgs::STAB_TESTDEVICE_CFG, '/cfgs/stab_testdevices', true, params)
  end

  #返回json
  get '/device_cfg_names' do
    @test_devices.map { |c| c[:cfg_name] }.to_json
  end
  get '/device_json' do
     @test_devices.find { |c| c[:cfg_name] == params[:cfg_name] }[:cfg].to_json
  end
end