DCWebServer::Cfgs.controllers do
  get '/drblist' do
    drblist_view(@server_pool, DCWebServer::Cfgs::DRUBY_CFG, '/cfgs/drblist', {:op => params[:op], :uri => params[:uri]})
  end

  get '/stab_drblist' do
    drblist_view(@stab_server_pool, DCWebServer::Cfgs::STAB_DRUBY_CFG, '/cfgs/stab_drblist', {:op => params[:op], :uri => params[:uri]})
  end

  ['/drblist', '/stab_drblist'].each do |url|
    post url do
      #检查是否合法
      error_msg = "输入格式错误!<a href='/cfgs/#{url}'>返回</a>"
      return error_msg unless params[:drb_uri] =~ %r{^druby://[a-zA-Z0-9.]+?:\d+/?$}
      case url
        when '/drblist'
          server_pool, cfg_file = @server_pool, DCWebServer::Cfgs::DRUBY_CFG
        when '/stab_drblist'
          server_pool, cfg_file = @stab_server_pool, DCWebServer::Cfgs::STAB_DRUBY_CFG
      end
      server_pool << params[:drb_uri]
      server_pool.sort!
      File.open(cfg_file, 'w') { |f| f.write server_pool.to_yaml }
      redirect "/cfgs#{url}"
    end
  end

  get '/json_list_drblist' do
    @server_pool.to_json
  end

  get '/json_list_stab_drblist' do
    @stab_server_pool.to_json
  end
end