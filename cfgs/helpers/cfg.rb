###################################
# 配置管理的一些辅助功能函数
##################################
DCWebServer::Cfgs.helpers do
  def drblist_view(server_pool, save_file, post_url, get_hash={})
    op = get_hash[:op]
    if op == 'del'
      uri = get_hash[:uri]
      server_pool.delete uri
      server_pool.sort!
      File.open(save_file, 'w') do |f|
        f.write server_pool.to_yaml
      end
    end
    erb :drblist, :locals => {:server_pool => server_pool, :post_url => post_url}
  end

  def testdevices_view(device_cfgs, save_file, post_url, stab = false, params={})
    op = params[:op]
    case op
      when 'del'
        device_cfgs.delete_if { |c| c[:cfg_name] == params[:cfg_name] }
        File.open(save_file, 'w') { |f| f.write device_cfgs.to_yaml }
      when 'add', 'edit'
        get_device_params(params) do |cfg|
          device_cfgs.delete_if { |c| c[:cfg_name] == cfg[:cfg_name] }
          device_cfgs << cfg
          device_cfgs = device_cfgs.sort { |a, b| a[:cfg_name] <=> b[:cfg_name] }
          File.open(save_file, 'w') { |f| f.write device_cfgs.to_yaml }
        end
    end
    erb :testdevices, :locals => {:device_cfgs => device_cfgs, :post_url => post_url, :stab => stab}
  end

  def get_device_params(params, &block)
    if  params[:cfg_name] =~ /.+/
      ret = {}
      params.each { |k, v| ret[k.to_sym] = v }
      cfg_name = ret[:cfg_name]
      params.delete(:op)
      params.delete(:cfg_name)
      cfg = {:cfg_name => cfg_name, :cfg => ret}
      yield cfg if block_given?
    end
  end

  def get_cfg_pool(yaml_file)
    cfg = []
    if File.exists?(yaml_file)
      if block_given?
        cfg = YAML.load_file(yaml_file).sort { |a, b| a[yield] <=> b[yield] } || []
      else
        cfg = YAML.load_file(yaml_file).sort || []
      end
    end
    cfg
  end
end
