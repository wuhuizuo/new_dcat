DCWebServer::Tools.controllers do
  post '/test_mysql' do
    host, db, mode = params[:mysql_host], params[:mysql_db], params[:mode]
    if  host && db && mode
      result = test_mysql(host, 'root', 'sinfors', db, {'outside' => 811, 'inside' => 3306}[mode])
      if (result == 'SUCCESS') then
        '<div class="alert alert-success">Ye. 连接成功了.</div>'
      else
        %{<div class="alert alert-danger">Sorry! 连接失败了（#{result}).</div>}
      end
    else
      '<div class="alert alert-warning">OH,NO! 缺少主机或者数据库或者DC模式参数！</div>'
    end
  end

  get '/test_mysql' do
    erb :test_mysql
  end
end