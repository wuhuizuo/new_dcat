DCWebServer::Tools.helpers do
  # *描述*:用于测试测试mysql是否可以访问
  def test_mysql(host, user, password, db, port)
    Timeout.timeout(3) do
      Mysql.real_connect(host, user, password, db, port) ? 'SUCCESS' : 'FAIL'
    end
  rescue Mysql::Error
    'ERROR'
  rescue Timeout::Error
    'Timeout'
  end

  #测试指定uri服务器地址是否在线
  def test_dr(uri)
    return 'ArgumentError' unless uri =~ %r{^druby://.+}
    timeout(1) { DRbObject.new_with_uri(uri).name }
    return 'online'
  rescue DRb::DRbConnError #服务没有启动,但机器已经在线
    return 'offline'
  rescue Timeout::Error #服务器没有开机
    return 'ip_error'
  end

  def save_screenshot(bin_content, dst_path)
    begin
      FileUtils.mkdir_p File.dirname(dst_path)
      File.open(dst_path, 'wb') { |f| f.write(bin_content) }
      dst_path
    rescue Exception
      nil
    end
  end
end

