require 'nokogiri'
DCWebServer::Tools.helpers do
#col_indexes:缺陷编号、概要、描述、研发备注
  def get_bugs_from_html(html, col_indexes=[0, 4, 5, 6])
    rows = Nokogiri::HTML(html).css('table')[0].xpath('//tr')
    rows[1..-1].map do |r|
      tds = r.xpath('td')
      id, title, desc, comment = col_indexes.map { |c_i| tds[c_i].inner_html }
      {:id => id, :title => title, :desc => desc, :comment => comment}
    end
  end
end