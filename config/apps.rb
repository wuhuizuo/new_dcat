##
# This file mounts each app in the Padrino project to a specified sub-uri.
# You can mount additional applications using any of these commands below:
#
#   Padrino.mount('blog').to('/blog')
#   Padrino.mount('blog', :app_class => 'BlogApp').to('/blog')
#   Padrino.mount('blog', :app_file =>  'path/to/blog/app.rb').to('/blog')
#
# You can also map apps to a specified host:
#
#   Padrino.mount('Admin').host('admin.example.org')
#   Padrino.mount('WebSite').host(/.*\.?example.org/)
#   Padrino.mount('Foo').to('/foo').host('bar.example.org')
#
# Note 1: Mounted apps (by default) should be placed into the project root at '/app_name'.
# Note 2: If you use the host matching remember to respect the order of the rules.
#
# By default, this file mounts the primary app which was generated with this project.
# However, the mounted app can be modified as needed:
#
#   Padrino.mount('AppName', :app_file => 'path/to/file', :app_class => 'BlogApp').to('/')
#

##
# Setup global project settings for your apps. These settings are inherited by every subapp. You can
# override these settings in the subapps as needed.
#
Padrino.configure_apps do
  enable :sessions
  set :session_secret, '5f2839e415bc7ffbae1a1cb11088afc50ec941203bf87e74bd9c216c0c7428d7'
  set :protection, :expect => :path_traversal
  set :protect_from_csrf, false
end

# Mounts the core application for this project
Padrino.mount('DCWebServer::Admin', :app_file => Padrino.root('admin/app.rb')).to('/admin')
Padrino.mount('DCWebServer::TcSnipper', :app_file => Padrino.root('tc_snipper/app.rb')).to('/tc_snipper')
Padrino.mount('DCWebServer::Reports', :app_file => Padrino.root('reports/app.rb')).to('/reports')
Padrino.mount('DCWebServer::Tools', :app_file => Padrino.root('tools/app.rb')).to('/tools')
Padrino.mount('DCWebServer::Docs', :app_file => Padrino.root('docs/app.rb')).to('/docs')
Padrino.mount('DCWebServer::Cfgs', :app_file => Padrino.root('cfgs/app.rb')).to('/cfgs')
Padrino.mount('DCWebServer::Monitors', :app_file => Padrino.root('monitors/app.rb')).to('/monitors')
Padrino.mount('DCWebServer::Tasks', :app_file => Padrino.root('tasks/app.rb')).to('/tasks')
Padrino.mount('DCWebServer::Tests', :app_file => Padrino.root('tests/app.rb')).to('/tests')
Padrino.mount('DCWebServer::App', :app_file => Padrino.root('app/app.rb')).to('/')
