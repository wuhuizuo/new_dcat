require 'drb'
require 'yaml'
module DCWebServer
  class Tasks < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    enable :sessions

    ##
    # Caching support.
    #
    # register Padrino::Cache
    # enable :caching
    #
    # You can customize caching store engines:
    #
    # set :cache, Padrino::Cache::Store::Memcache.new(::Memcached.new('127.0.0.1:11211', :exception_retry_limit => 1))
    # set :cache, Padrino::Cache::Store::Memcache.new(::Dalli::Client.new('127.0.0.1:11211', :exception_retry_limit => 1))
    # set :cache, Padrino::Cache::Store::Redis.new(::Redis.new(:host => '127.0.0.1', :port => 6379, :db => 0))
    # set :cache, Padrino::Cache::Store::Memory.new(50)
    # set :cache, Padrino::Cache::Store::File.new(Padrino.root('tmp', app_name.to_s, 'cache')) # default choice
    #

    ##
    # Application configuration options.
    #
    # set :raise_errors, true       # Raise exceptions (will stop application) (default for test)
    # set :dump_errors, true        # Exception backtraces are written to STDERR (default for production/development)
    # set :show_exceptions, true    # Shows a stack trace in browser (default for development)
    # set :logging, true            # Logging in STDOUT for development and file for production (default only for development)
    # set :public_folder, 'foo/bar' # Location for static assets (default root/public)
    # set :reload, false            # Reload application files (default in development)
    # set :default_builder, 'foo'   # Set a custom form builder (default 'StandardFormBuilder')
    # set :locale_path, 'bar'       # Set path for I18n translations (default your_apps_root_path/locale)
    # disable :sessions             # Disabled sessions by default (enable if needed)
    # disable :flash                # Disables sinatra-flash (enabled by default if Sinatra::Flash is defined)
    # layout  :my_layout            # Layout can be in views/layouts/foo.ext or views/foo.ext (default :application)
    #
    layout :'../../app/views/layouts/application'
    TC_DATA_PARENT_DIR = File.join(File.dirname(__FILE__), '..', 'tests')
    ##
    # You can configure for a specified environment like:
    #
    #   configure :development do
    #     set :foo, :bar
    #     disable :asset_stamp # no asset timestamping for dev
    #   end
    #

    ##
    # You can manage errors like:
    #
    #   error 404 do
    #     render 'errors/404'
    #   end
    #
    #   error 505 do
    #     render 'errors/505'
    #   end
    #
    get '/add_task' do
      erb :add_task
    end

    get '/add_stab_task' do
      erb :add_task
    end

    post '/add_task' do      
      #获取测试执行配置
      run_config = {} 
      YAML.load(params[:cfg].to_yaml).each { |k,v|    run_config[k.to_sym] = v }
      run_config[:dc_system_config] = {}
      if run_config[:sys_config]
        kvs = run_config[:sys_config].strip.split(%r{\r\n|\n}).map { |kv| kv.split(%r{\s*:\s*}) }
        run_config[:dc_system_config] = Hash[kvs]
      end
      result_dir = run_config[:result_dir]
      run_config[:result_dir] = (result_dir =~ %r{^result/} ? result_dir : "result/#{result_dir}")
      
      #获取执行机器地址
      drb_uri = run_config[:drb_uri]
      run_config.delete(drb_uri)

      #获取测试执行用例
      testcases = []
      YAML.load(params[:tcs].to_yaml).each do |type, suites|
        suites.each do |suite_name, tcs|
          tcs.map!{|id| YAML.load_file File.join(__dir__, '..', 'tests', "#{type}_data", suite_name, "#{id}.yaml") }
          testcases << {:type => type, :tests => tcs, :name => suite_name }
        end        
      end
      
      dr = DRbObject.new_with_uri(drb_uri)
      dr.run(run_config, testcases)
      "/monitors/running?uri=#{drb_uri}"
    end

    post '/add_stab_task' do
      run_config = params.select{|k,_| k =~ /^(web_|mysql_|mode|debug_sql_level)/}
      run_config[:run_times] = params[:run_times].to_i
      run_config[:dc_system_config] = {}
      if params[:sys_config]
        kvs = params[:sys_config].strip.split(%r{\r\n|\n}).map { |kv| kv.split(%r{\s*:\s*}) }
        run_config[:dc_system_config] = Hash[kvs]
      end
      if params[:result_dir] =~ %r{^result/}
        run_config[:result_dir] = params[:result_dir]
      else
        run_config[:result_dir] = 'result/' + params[:result_dir]
      end
      run_config[:debug_sql_level] = params[:debug_sql_level]
      drb_uri = params[:drb_uri]

      testcases = []
      params.each do |k, v|
        next unless k =~ /cases_of_\w+/
        type, suite_name = k.split('_cases_of_')
        tests = v.map { |c| YAML.load_file "#{TC_DATA_PARENT_DIR}/#{type}_data/#{suite_name}/#{c}.yaml" }
        testcases << {:type => type, :name => suite_name, :query_case => tests} if tests.length > 0
      end

      dr = DRbObject.new_with_uri(drb_uri)
      dr.run(run_config, testcases)
      redirect "/monitors/running?uri=#{drb_uri}"
    end
  end
end
