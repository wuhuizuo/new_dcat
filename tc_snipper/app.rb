require 'json'
require 'yaml'

module DCWebServer
  class TcSnipper < Padrino::Application
    def initialize
      @glob_funcs = YAML.load_file GLOB_FUNCS_YAML
      @object_funcs = YAML.load_file OBJECT_FUNCS_YAML
      super
    end

    GLOB_FUNCS_YAML = File.join(File.dirname(__FILE__), 'config', 'glob_func.yaml')
    OBJECT_FUNCS_YAML = File.join(File.dirname(__FILE__), 'config', 'object_func.yaml')


    register Padrino::Rendering
    register Padrino::Helpers
    enable :sessions

    ##
    # Caching support.
    #
    # register Padrino::Cache
    # enable :caching
    #
    # You can customize caching store engines:
    #
    # set :cache, Padrino::Cache::Store::Memcache.new(::Memcached.new('127.0.0.1:11211', :exception_retry_limit => 1))
    # set :cache, Padrino::Cache::Store::Memcache.new(::Dalli::Client.new('127.0.0.1:11211', :exception_retry_limit => 1))
    # set :cache, Padrino::Cache::Store::Redis.new(::Redis.new(:host => '127.0.0.1', :port => 6379, :db => 0))
    # set :cache, Padrino::Cache::Store::Memory.new(50)
    # set :cache, Padrino::Cache::Store::File.new(Padrino.root('tmp', app_name.to_s, 'cache')) # default choice
    #

    ##
    # Application configuration options.
    #
    # set :raise_errors, true       # Raise exceptions (will stop application) (default for test)
    # set :dump_errors, true        # Exception backtraces are written to STDERR (default for production/development)
    # set :show_exceptions, true    # Shows a stack trace in browser (default for development)
    # set :logging, true            # Logging in STDOUT for development and file for production (default only for development)
    # set :public_folder, 'foo/bar' # Location for static assets (default root/public)
    # set :reload, false            # Reload application files (default in development)
    # set :default_builder, 'foo'   # Set a custom form builder (default 'StandardFormBuilder')
    # set :locale_path, 'bar'       # Set path for I18n translations (default your_apps_root_path/locale)
    # disable :sessions             # Disabled sessions by default (enable if needed)
    # disable :flash                # Disables sinatra-flash (enabled by default if Sinatra::Flash is defined)
    #layout :'layouts/default' # Layout can be in views/layouts/foo.ext or views/foo.ext (default :application)
    #
    layout :'../../app/views/layouts/application'

    ##
    # You can configure for a specified environment like:
    #
    #   configure :development do
    #     set :foo, :bar
    #     disable :asset_stamp # no asset timestamping for dev
    #   end
    #

    ##
    # You can manage errors like:

    error(404) { render 'errors/404' }
    error(505) { render 'errors/505' }

    post('/') do
      msg = parse_context(params[:context])
      msg['head'] + "\n" + msg['body'].map(&:to_s).join("\n")
    end

    post '/steps_to_code' do
      step_context = params[:steps]
      context_to_code(step_context)
    end

    post('/json_list') do
      msg = parse_context(params[:context])
      JSON.unparse msg['body'].map(&:to_s)
    end

    get '/config' do
      erb :'config/index'
    end

    get '/config/glob' do
      erb :'config/glob', :layout => false, :locals => {:glob_funcs => @glob_funcs}
    end

    post '/config/glob' do
      begin
        update_glob(params)
        redirect '/tc_snipper/config'
      rescue Exception => e
        erb :'errors/run_error', :locals => {:content => e.message}
      end
    end

    get '/config/object' do
      erb :'config/object', :layout => false, :locals => {:classes => @object_funcs.keys}
    end

    post '/config/object' do
      begin
        update_object(params)
        'success!'
      rescue Exception => e
        erb :'errors/run_error', :locals => {:content => e.message}
      end
    end

    get '/config/object_class' do
      @object_funcs.keys.map { |k| "<option>#{k}</option>" }.join("\n")
    end

    get '/config/object_func' do
      klass = params[:klass]
      get_object_funcs(klass).map { |f| "<option value='#{@object_funcs[klass][f]['code']}'>#{f}</option>" }.join("\n")
    end

    get '/config/object_func_param' do
      arguments = get_object_func_params(params[:klass], params[:func])
      arguments.map { |a| "<option>#{a}</option>" }.join("\n")
    end

    get '/debug' do
      codes = context_to_code(params[:tc])
      puts codes.inspect
      erb :debug, :locals => {:code => codes, :tc => params[:tc]}
    end
  end
end
