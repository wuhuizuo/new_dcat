DCWebServer::TcSnipper.helpers do
  def add_glob_func(func_name)
    @glob_funcs[func_name] = {}
  end

  def del_glob_func(func_name)
    @glob_funcs.delete(func_name)
    save_glob_funcs
  end

  def add_glob_func_param(func_name, param, klass)
    add_glob_func(func_name) unless @glob_funcs[func_name]
    if @glob_funcs[func_name].keys.include? klass
      raise "Conflict class name:#{klass}, it already exists!"
    end
    if @glob_funcs[func_name].values.include? param
      raise "Conflict func param:#{param}, it already exists!"
    end
    @glob_funcs[func_name][klass] = (eval(param) || {})
    save_glob_funcs
  end

  def del_glob_func_param(func_name, param, klass)
    param = eval(param) || {}
    @glob_funcs[func_name].delete_if { |k, v| k == klass and v == param }
    del_glob_func(func_name) unless @glob_funcs[func_name] && (@glob_funcs[func_name].length > 0)
    save_glob_funcs
  end

  def add_object_class(klass)
    raise "Conflict class name:#{klass}, it already exists!" if @object_funcs.key?(klass)
    @object_funcs[klass] = {}
    save_object_funcs
  end

  def del_object_class(klass)
    raise "Object :#{klass}, does not exist!" unless @object_funcs.key?(klass)
    @object_funcs.delete(klass)
    save_object_funcs
  end

  def add_object_func(klass, natrue_func, code_func)
    if @object_funcs[klass].key?(natrue_func)
      raise "Conflict opr_func name:#{klass}##{natrue_func}, it already exists!"
    end
    @object_funcs[klass][natrue_func] = {'code' => code_func, 'params' => []}
    save_object_funcs
  end

  def del_object_func(klass, natrue_func)
    unless @object_funcs[klass].key?(natrue_func)
      raise "Object opr_func name:#{klass}##{natrue_func} not found!"
    end
    @object_funcs[klass].delete(natrue_func)
    save_object_funcs
  end

  def add_object_param(klass, natrue_func, param)
    unless @object_funcs[klass][natrue_func]
      raise "Object opr_func name:#{klass}##{natrue_func} not found, or struct error!"
    end
    param = eval param
    @object_funcs[klass][natrue_func]['params'] << param
    save_object_funcs
  end

  def del_object_param(klass, natrue_func, param)
    unless @object_funcs[klass][natrue_func]
      raise "Object opr_func name:#{klass}##{natrue_func} not found, or struct error!"
    end
    param = eval param
    @object_funcs[klass][natrue_func]['params'].delete param
    save_object_funcs
  end

  def save_glob_funcs
    File.open(DCWebServer::TcSnipper::GLOB_FUNCS_YAML, 'w') { |f| f.write @glob_funcs.to_yaml }
  end

  def save_object_funcs
    File.open(DCWebServer::TcSnipper::OBJECT_FUNCS_YAML, 'w') { |f| f.write @object_funcs.to_yaml }
  end

  def update_glob(params)
    case params[:opr]
      when 'add'
        add_glob_func_param(params[:func], params[:func_param], params[:func_class])
      when 'edit'
      when 'del'
        del_glob_func_param(params[:func], params[:func_param], params[:func_class])
      else
        raise ArgumentError, "Not supported param #{params[:opr]}"
    end
  end

  def update_object(params)
    opr_data = []
    case params[:opr]
      when 'add_object_class', 'del_object_class'
        opr_data << params[:klass]
      when 'del_object_func'
        opr_data << params[:klass] << params[:func]
      when 'add_object_func'
        opr_data << params[:klass] << params[:func] << params[:code]
      when 'add_object_param', 'del_object_param'
        opr_data << params[:klass] << params[:func] << params[:param]
      else
        raise "Not supported operation:#{params[:opr]} !"
    end
    send(params[:opr], *opr_data)
  end
end