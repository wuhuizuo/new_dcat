DCWebServer::TcSnipper.helpers do
  def get_glob_funcs
    @glob_funcs.keys
  end

  def get_glob_func_params(func_name)
    @glob_funcs[func_name].values
  end

  def get_object(glob_op, op_params=nil)
    klass_hash = @glob_funcs[glob_op]
    klasses = klass_hash.keys rescue []
    klass = klasses.find { |k| op_params == klass_hash[k] }
    unless klass
      klass = klasses.find { |k|
        klass_hash[k].respond_to?(:each) and op_params and
            klass_hash[k].all? { |key, v| v == op_params[key] }
      }
    end
    klass || 'Unkown'
  end


  def get_object_funcs(object_name)
    if object_name
      @object_funcs[object_name].keys rescue []
    else
      []
    end
  end

  def get_object_func_code(object_name, natrue_func_name)
    return nil unless  object_name and natrue_func_name
    @object_funcs[object_name][natrue_func_name]['code'] rescue 'unknow'
  end

  def get_object_func_params(object_name, natrue_func_name)
    return nil unless  object_name and natrue_func_name
    @object_funcs[object_name][natrue_func_name]['params'] rescue []
  end


  def parse_context(context)
    glob_func_reg = %r{#(\w+):(\S*)}
    glob_func_param_reg = %r{#(\w+):(\S+)\s+}
    object_func_reg = %r{#(\w+)\.(\S*)}
    object_func_param_reg = %r{#(\w+)\.(\S+)\s+}

    lines = context.split("\n")
    last_line = lines[-1]
    case last_line
      when glob_func_param_reg
        func = last_line.match(glob_func_param_reg)[2]
        tip_glob_func_params(func)
      when glob_func_reg
        func_prefix = last_line.match(glob_func_reg)[2]
        tip_glob_funcs(func_prefix)
      when object_func_param_reg
        step_obj, func = last_line.match(object_func_reg)[1..2]
        code_klass = get_object_by_context(lines, step_obj)
        tip_object_func_params(code_klass, step_obj, func)
      when object_func_reg
        step_obj, func_prefix = last_line.match(object_func_reg)[1..2]
        code_klass = get_object_by_context(lines, step_obj)
        tip_object_funcs(code_klass, step_obj, func_prefix)
      else
        {'head' => 'Sorry:', 'body' => ['这里没啥可提示的^@^,这步骤少了啥?']}
    end
  rescue Exception => e
    {'head' => 'Error:', 'body' => [e.message]}
  end

  def context_to_code(context)
    glob_func_reg = %r{#(\w+):(\S+)(\s+.+)?$}
    object_func_reg = %r{(.*?)#(\w+).(\S+)(\s+.+)?$}
    ret = []
    #去掉注释信息
    lines = context.to_s.gsub(%r{//.*}, '').split(%r{[\r\n]+})
    lines.delete('')
    lines.each_with_index do |line, l_index|
      case line
        when glob_func_reg
          ret << glob_step_to_code(glob_func_reg, line)
        when object_func_reg
          ret << object_step_to_code(line, lines[0..l_index], object_func_reg)
        else
          ret << line
      end
    end
    ret = exec_steps_to_code ret
    ret = assert_steps_to_code ret
    ret.join("\n")
  rescue Exception => e
    ret.join("\n") + "\n代码翻译失败:\n" + e.message + e.backtrace.join("\n")
  end

  def glob_step_to_code(line, glob_func_reg)
    #puts "glob_step_to_code:#{line}"
    m_ret = line.match(glob_func_reg)
    obj, glob_func, glob_param = m_ret[1..3]
    glob_param = glob_param.sub(/^\s+/, '') if glob_param
    klass = get_object(glob_func, eval(glob_param))
    code = "obj_#{obj} = #{klass}.new"
    code << '(' << add_params_for_dcweb_glob(glob_param) << ')' if klass =~ /^DCWEB::/
    code
  end

  def object_step_to_code(line, pre_lines, object_func_reg)
    puts "glob_step_to_code:#{line}"
    m_ret = line.match(object_func_reg)
    pref, obj, func, param = m_ret[1..4]
    param = param.sub(/\s+/, '') if param
    obj_class = get_object_by_context(pre_lines, obj)
    pref + format('obj_%s.%s(%s)', obj, get_object_func_code(obj_class, func), param)
  end

  def exec_steps_to_code(lines)
    exec_reg = %r{^(\s*!\S+\s*\|)?\s*:(.+)}
    lines.map do |line|
      mat_ret = line.match exec_reg
      mat_ret ? mat_ret[1..2].join : line
    end
  end

  def assert_steps_to_code(lines)
    lines.map { |line| assert_step_to_code line }
  end

  def assert_step_to_code(line)
    assert_reg = %r{^!(\S+)\s*\|\s*(.*)}
    mat_ret = line.match(assert_reg)
    return line unless mat_ret
    assert_flag, assert_step = mat_ret[1..2]
    case assert_flag
      when /^fail$/i, '失败'
        'expect_fail { ' + assert_step + ' }'
      when /^pass$/i, '成功'
        assert_step
      when /^assert$/i, '断言', '判断'
        'assert { ' + assert_step + ' }'
      else
        raise "不支持的断言类型:#{assert_flag}"
    end
  end

  def add_params_for_dcweb_glob(glob_param)
    fake_glob_param = glob_param.gsub(/{(\s*['|"])|(['|"]\s*}\s*)/, '')
    keys = fake_glob_param.split(/\s*,\s*['|"]/).map { |kv| kv.split(/['|"]\s*=>\s*/)[0] }
    keys.include?('浏览器') ? glob_param : glob_param.sub('}', ', ' + '"浏览器" => browser }')
  end

#解析step_obj上下文对应对象的初始语句
  def get_object_by_context(context_lines, step_obj)
    #查找step_object上下文对应上下文对象的初始语句
    init_line = context_lines.reverse.find { |e| e =~ %r{##{step_obj}:} }
    raise "对象:#{step_obj} 还没有初始化吧!检查下?" unless init_line
    glob_func, glob_param = init_line.match(%r{##{step_obj}:(\S+)\s*(.*)})[1..2]
    glob_param = eval glob_param
    glob_param = {} unless glob_param.respond_to?(:each)
    get_object(glob_func, glob_param)
  end


  def tip_glob_funcs(prefix_str)
    match_funcs = get_glob_funcs.select { |f| f =~ %r{^#{prefix_str}} }
    {
        'head' => "#全局操作输入模式\n当前可选或匹配的操作列表:",
        'body' => match_funcs
    }
  end

  def tip_glob_func_params(func)
    params = get_glob_func_params(func)
    {
        'head' => "#全局操作参数输入模式\n当前可选的参数列表:",
        'body' => params
    }
  end

  def tip_object_funcs(code_klass, step_object, prefix_str)
    match_funcs = get_object_funcs(code_klass).select { |f| f =~ %r{^#{prefix_str}} }
    {
        'head' => "#对象操作输入模式\n[#{step_object}]当前可选或匹配的操作列表:",
        'body' => match_funcs
    }
  end

  def tip_object_func_params(code_klass, step_object, func)
    params = get_object_func_params(code_klass, func)
    {
        'head' => "#对象操作参数输入模式\n[#{step_object}.#{func}]参数举例:",
        'body' => params
    }
  end

end



